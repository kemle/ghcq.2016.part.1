# README #

### What is this repository for? ###

* Solver for the 2016 GHCQ Part 1 Puzzle
* Version 1

### How do I get set up? ###

* Python3
* Used to solve this puzzle:
In this type of grid-shading puzzle, each square is either black or white. Some of the black squares have already been filled in for you.
Each row or column is labelled with a string of numbers. The numbers indicate the length of all consecutive runs of black squares, and are displayed in the order that the runs appear in that line. For example, a label "2 1 6" indicates sets of two, one and six black squares, each of which will have at least one white square separating them.

![gchq-puzzle.jpeg](https://bytebucket.org/kemle/ghcq.2016.part.1/raw/3dc26826afc08bd785c6f5827de53c039e3caee4/gchq-puzzle.jpeg)

### Who do I talk to? ###

* Contact me through www.kemle.com contact form
