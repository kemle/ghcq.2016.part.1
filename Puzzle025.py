#! /usr/local/bin/python3
"""
Program: Puzzle025.py
Solver for GCHQ Puzzle Part 1

The MIT License (MIT)

Copyright (c) 2016 Bill Kemle

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

from graphics import GraphWin
from graphics import Rectangle
from graphics import Point
from graphics import time
from threading import Thread
import itertools
import multiprocessing

# Define Global Constants
GRID_BLACK = 0  # class enumerations are slow
GRID_WHITE = 1
# Assuming grid is square 25x25
GRID_SIZE = 25
CPU_COUNT = 3


def x_reduce_solution_set_wrapper(grid, listOfXPatterns, q):
    listOfXPatterns = x_reduce_solution_set(grid, listOfXPatterns)
    q.put(listOfXPatterns)
    return


def y_reduce_solution_set_wrapper(grid, listOfYPatterns, q):
    listOfYPatterns = y_reduce_solution_set(grid, listOfYPatterns)
    q.put(listOfYPatterns)
    return


def x_build_space_patterns_wrapper(xWordCount, xExtraSpaces, xClue, listOfXPatterns, x):
    listOfXPatterns.append(build_space_patterns(x, xWordCount, xExtraSpaces, xClue))
    return (x, listOfXPatterns[0])


def y_build_space_patterns_wrapper(yWordCount, yExtraSpaces, yClue, listOfYPatterns, y):
    listOfYPatterns.append(build_space_patterns(y, yWordCount, yExtraSpaces, yClue))
    return (y, listOfYPatterns[0])


def partition(n, t, l):
    """ Partitions of n.

    The partitions of 4 are 1+1+1+1, 1+1+2, 2+2, 1+3, and 4.
    n=number, t=total, l=length
    """
    if n == 0:  # check if zero and return empty set
        q = list()
        if n == t:  # add zeros
            [q.append(0) for i in range(l)]
        yield q
        return

    for p in partition(n - 1, t, l):  # modify partitions of n-1 to form partitions of n
        p.append(1)
        if n == t:
            a = l - len(p)
            [p.append(0) for i in range(len(p), l)]  # add zeros
            yield p
            [p.pop() for i in range(a)]  # remove zeros
        else:
            yield p
        p.pop()
        if p and (len(p) < 2 or p[-2] > p[-1]):
            p[-1] += 1
            if n == t:
                a = l - len(p)
                [p.append(0) for i in range(len(p), l)]  # add zeros
                yield p
                [p.pop() for i in range(a)]  # remove zeros
            else:
                yield p


def mark_solved_cells(possibleXSolutions, possibleYSolutions, grid):
    """ mark_solved_cells checks for each row and each column if an individual
    cell has either all black or all white solutions.  If all of the solutions
    for a cell, in either direction, are the same, then that must be the cell's
    final state.
    """
    solutionsFound = 0
    for row in range(GRID_SIZE):
        lenOfRow = len(possibleXSolutions[row])
        for cell in range(GRID_SIZE):
            averageOfAllSolutionsForaCell = 0
            if lenOfRow > 0:
                for possibleXSolution in range(lenOfRow):
                    averageOfAllSolutionsForaCell += possibleXSolutions[row][possibleXSolution][cell]
                averageOfAllSolutionsForaCell = averageOfAllSolutionsForaCell / lenOfRow
                if averageOfAllSolutionsForaCell == 0:  # every solution 0
                    grid[cell][row][GRID_WHITE] = True
                    solutionsFound += 1
                elif averageOfAllSolutionsForaCell == 1:  # every solution 1
                    grid[cell][row][GRID_BLACK] = True
                    solutionsFound += 1

    for row in range(GRID_SIZE):
        lenOfRow = len(possibleYSolutions[row])
        for cell in range(GRID_SIZE):
            averageOfAllSolutionsForaCell = 0
            if lenOfRow > 0:
                for possibleYSolution in range(lenOfRow):
                    averageOfAllSolutionsForaCell += possibleYSolutions[row][possibleYSolution][cell]
                averageOfAllSolutionsForaCell = averageOfAllSolutionsForaCell / lenOfRow
                if averageOfAllSolutionsForaCell == 0:  # every solution 0
                    grid[row][cell][GRID_WHITE] = True
                    solutionsFound += 1
                elif averageOfAllSolutionsForaCell == 1:  # every solution 1
                    grid[row][cell][GRID_BLACK] = True
                    solutionsFound += 1

    return solutionsFound


def x_reduce_solution_set(grid, listOfXPatterns):
    """ Go through all of the possible solutions for each row and column.
    Compare against the knowns and weed out impossible solutions.
    """
    xSolutions = [list() for x in range(GRID_SIZE)]
    isGuessValid, item = False, 0
    guess = list()

    for rowCol in range(GRID_SIZE):
        guess = listOfXPatterns[rowCol]
        for item in range(len(guess)):
            isGuessValid = True  # weed out guesses that dont match the known cell values
            for cell in range(GRID_SIZE):
                if(((grid[cell][rowCol][GRID_BLACK]) and guess[item][cell] != 1) or
                   ((grid[cell][rowCol][GRID_WHITE]) and guess[item][cell] != 0)):
                    isGuessValid = False
            if isGuessValid:
                xSolutions[rowCol].append(guess[item])

    return xSolutions


def y_reduce_solution_set(grid, listOfYPatterns):
    """ A separate function is created to separate the memory space for threading """
    ySolutions = [list() for x in range(GRID_SIZE)]
    isGuessValid, item = False, 0
    guess = list()

    for rowCol in range(GRID_SIZE):
        guess = listOfYPatterns[rowCol]
        for item in range(len(guess)):
            isGuessValid = True
            for cell in range(GRID_SIZE):  # weed out guesses that dont match the known cell values
                if(((grid[rowCol][cell][GRID_BLACK]) and guess[item][cell] != 1) or
                   ((grid[rowCol][cell][GRID_WHITE]) and guess[item][cell] != 0)):
                    isGuessValid = False
            if isGuessValid:
                ySolutions[rowCol].append(guess[item])

    return ySolutions


def convert_space_pattern_to_grid_pattern(rowCol, wordCount, clue, tempList):
    """ Use a clue like [7,2,1,2,5] and a space pattern (2, 1, 0, 0, 0, 4, 2)
    to convert into:
    [1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0]
    """
    position, word = 0, 0
    gridPattern = [0] * GRID_SIZE

    for word in range(wordCount):
        if word > 0:  # add the expected space before all but the first word
            gridPattern[position] = 0
            position += 1
        for space in range(tempList[word]):  # add the extra spaces
            gridPattern[position] = 0
            position += 1
        for letter in range(clue[word][rowCol]):  # fill in the letters
            gridPattern[position] = 1
            position += 1

    return gridPattern


def build_space_patterns(rowCol, wordCount, extraSpaces, clue):
    """ The number of extra white cells that can be put between black cells has been calculated.

    Use this to generate all of the possible permutations of these patterns by
    figuring out how to disribute the white cells between the black.
    4 extra white cells could generate:
    [1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0] or
    [1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0] for example.
    """
    numberOfWordsInTheClue = wordCount[rowCol]
    spacePattern = []
    partitionedClue = 0
    # turn 5, 5, 7 into (1, 1, 1, 1, 0, 1, 0), (1, 1, 1, 1, 0, 0, 1) etc
    filteredSet = set()  # use a set to filter out duplicates
    for partitionedClue in partition(extraSpaces[rowCol], extraSpaces[rowCol], numberOfWordsInTheClue + 1):  # 4: 4, 3+1, 2+2, 1+1+1+1
        if sum(itertools.islice(partitionedClue, numberOfWordsInTheClue + 1, GRID_SIZE)) == 0:
            [filteredSet.add(item) for item in itertools.permutations(partitionedClue)]  # find all of the permutations

    [spacePattern.append(convert_space_pattern_to_grid_pattern(rowCol, numberOfWordsInTheClue, clue, item)) for item in filteredSet]

    return spacePattern


def extra_spaces(xExtraSpaces, yExtraSpaces, xClue, yClue, xWordCount, yWordCount):
    """ Calculate the number of extra white spaces available on each row or column """
    i, j = 0, 0
    # Count the number of extra spaces available
    for j in range(GRID_SIZE):
        for i in range(10):  # 10 is the max clue length
            xExtraSpaces[j] = xExtraSpaces[j] + xClue[i][j]
            yExtraSpaces[j] = yExtraSpaces[j] + yClue[i][j]

            if xClue[i][j]:
                xExtraSpaces[j] = xExtraSpaces[j] + 1
                xWordCount[j] = xWordCount[j] + 1

            if yClue[i][j]:
                yExtraSpaces[j] = yExtraSpaces[j] + 1
                yWordCount[j] = yWordCount[j] + 1

        xExtraSpaces[j] = GRID_SIZE + 1 - xExtraSpaces[j]
        yExtraSpaces[j] = GRID_SIZE + 1 - yExtraSpaces[j]


def paint_canvas(win, grid, canvas, repaint=False):
    """ Create the Puzzle GUI """
    xSize, ySize = 10, 10
    xOffset, yOffset = 10, 10
    x, y = 0, 0
    for y in range(GRID_SIZE):
        for x in range(GRID_SIZE):
            if not repaint:
                canvas[x][y] = Rectangle(Point(xOffset + x * xSize, yOffset + y * ySize),
                                         Point(xOffset + x * xSize + xSize, yOffset + y * ySize + ySize))
                canvas[x][y].draw(win)
            if grid[x][y][GRID_BLACK]:
                canvas[x][y].setFill("black")
            elif grid[x][y][GRID_WHITE]:
                canvas[x][y].setFill("white")
            elif not repaint:
                canvas[x][y].setFill("grey")
    win.flush()


def main():
    startTime = time.time()

    # Setup the GUI
    win = GraphWin('Puzzle', 270, 270)
    win.yUp()
    rect = Rectangle(Point(10, 10), Point(260, 260))
    rect.draw(win)
    win.autoflush = False  # only update the GUE when told

    # Initialize variables
    # Black is a filled in black square
    BLACK = True
    # Grey square can be either black or white
    GRAY = False
    # Initialize all sqaures in the gride to GRAY
    # grid[x][y][color]
    grid = [[[GRAY, GRAY] for x in range(GRID_SIZE)] for y in range(GRID_SIZE)]
    # Mark all of the predifined BLACK squares
    grid[3][3][0], grid[4][3][0], grid[9][3][0], grid[10][3][0], grid[15][3][0], grid[20][3][0], grid[21][3][0] = BLACK, BLACK, BLACK, BLACK, BLACK, BLACK, BLACK
    grid[6][8][0], grid[11][8][0], grid[16][8][0], grid[20][8][0] = BLACK, BLACK, BLACK, BLACK
    grid[6][16][0], grid[7][16][0], grid[10][16][0], grid[14][16][0], grid[15][16][0], grid[18][16][0] = BLACK, BLACK, BLACK, BLACK, BLACK, BLACK
    grid[3][21][0], grid[4][21][0], grid[12][21][0], grid[13][21][0], grid[21][21][0] = BLACK, BLACK, BLACK, BLACK, BLACK

    # Clues are mapped down.  The origin is assumed lower left.  First X clue is 7 2 1 2 5.
    xClue = [[7, 1, 1, 1, 1, 1, 7, 1, 3, 2, 1, 3, 2, 1, 4, 1, 1, 3, 7, 1, 1, 1, 1, 1, 7],
             [2, 1, 3, 3, 3, 1, 1, 2, 1, 1, 9, 2, 1, 1, 1, 1, 2, 3, 1, 1, 3, 3, 3, 1, 3],
             [1, 2, 1, 1, 1, 2, 2, 2, 1, 2, 1, 2, 1, 1, 4, 3, 3, 0, 1, 2, 1, 1, 1, 2, 1],
             [2, 1, 1, 3, 4, 1, 1, 5, 1, 2, 1, 6, 1, 1, 2, 2, 1, 0, 1, 1, 5, 1, 3, 2, 1],
             [5, 1, 6, 10, 5, 2, 1, 0, 1, 3, 2, 3, 2, 1, 1, 1, 1, 0, 1, 1, 2, 6, 1, 1, 7],
             [0, 2, 6, 2, 1, 2, 1, 0, 5, 1, 1, 1, 5, 4, 2, 1, 3, 0, 1, 0, 1, 1, 1, 1, 0],
             [0, 0, 0, 0, 0, 1, 3, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 7, 0, 3, 3, 3, 0, 0],
             [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0],
             [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0],
             [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]

    # Y clues are mapped down in the matrix, but starting from the origin (lower left) moving up.
    # First clue 71127
    yClue = [[7, 1, 1, 1, 1, 1, 7, 3, 1, 2, 1, 1, 6, 1, 2, 1, 1, 1, 3, 4, 1, 4, 3, 1, 1],
             [1, 1, 3, 3, 3, 1, 1, 1, 2, 1, 2, 1, 2, 3, 2, 2, 8, 2, 1, 1, 7, 1, 3, 6, 1],
             [1, 2, 1, 1, 1, 2, 1, 1, 8, 1, 3, 1, 1, 1, 5, 1, 1, 6, 1, 1, 3, 1, 4, 2, 2],
             [2, 2, 3, 5, 4, 1, 1, 0, 1, 1, 7, 1, 1, 1, 2, 1, 2, 0, 4, 1, 1, 2, 1, 2, 3],
             [7, 1, 1, 1, 1, 1, 1, 0, 2, 2, 1, 1, 4, 1, 1, 1, 3, 0, 1, 1, 3, 1, 3, 2, 1],
             [0, 1, 3, 1, 1, 1, 1, 0, 1, 1, 0, 3, 0, 3, 0, 1, 3, 0, 7, 0, 1, 1, 1, 1, 7],
             [0, 0, 1, 3, 3, 0, 7, 0, 2, 2, 0, 2, 0, 3, 0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 0],
             [0, 0, 3, 1, 1, 0, 0, 0, 0, 2, 0, 1, 0, 0, 0, 2, 0, 0, 0, 0, 0, 3, 0, 0, 0],
             [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 1, 0, 0, 0],
             [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]

    xExtraSpaces = [0] * GRID_SIZE  # how many extra spaces are available on each line
    yExtraSpaces = [0] * GRID_SIZE
    xWordCount = [0] * GRID_SIZE  # The number of words in each clue
    yWordCount = [0] * GRID_SIZE

    extra_spaces(xExtraSpaces, yExtraSpaces, xClue, yClue, xWordCount, yWordCount)

    listOfXPatterns = []
    listOfYPatterns = []

    canvas = [[0 for x in range(GRID_SIZE)] for y in range(GRID_SIZE)]

    # solve the puzzle
    print("Genrating Patterns")

    pool = multiprocessing.Pool(processes=CPU_COUNT)
    results1 = [pool.apply_async(x_build_space_patterns_wrapper,
                                 args=(xWordCount, xExtraSpaces, xClue, listOfXPatterns, x)
                                 ) for x in range(GRID_SIZE)]
    results2 = [pool.apply_async(y_build_space_patterns_wrapper,
                                 args=(yWordCount, yExtraSpaces, yClue, listOfYPatterns, x)
                                 ) for x in range(GRID_SIZE)]

    paint_canvas(win, grid, canvas)

    listOfXPatterns = [r.get() for r in results1]  # getting responses in random order
    listOfXPatterns.sort()  # sort on r[0]
    listOfXPatterns = [r[1] for r in listOfXPatterns]  # pull values from r[1]

    listOfYPatterns = [r.get() for r in results2]
    listOfYPatterns.sort()
    listOfYPatterns = [r[1] for r in listOfYPatterns]

    print("Patterns:", sum([len(x) for x in listOfXPatterns]) +
          sum([len(x) for x in listOfYPatterns]))
    print("setup Time:", time.time() - startTime, "\n")

    solutionCount = mark_solved_cells(listOfXPatterns, listOfYPatterns, grid)
    print("solved", round(solutionCount / 2), "of", GRID_SIZE * GRID_SIZE, "cells")

    queue1 = multiprocessing.Queue()
    queue2 = multiprocessing.Queue()

    while (solutionCount < GRID_SIZE * GRID_SIZE * 2):

        thread1 = Thread(target=x_reduce_solution_set_wrapper,
                         args=(grid, listOfXPatterns, queue1))
        thread1.start()

        thread2 = Thread(target=y_reduce_solution_set_wrapper,
                         args=(grid, listOfYPatterns, queue2))
        thread2.start()

        paint_canvas(win, grid, canvas, repaint=True)

        listOfXPatterns = queue1.get(listOfXPatterns)
        listOfYPatterns = queue2.get(listOfYPatterns)

        solutionCount = mark_solved_cells(listOfXPatterns, listOfYPatterns, grid)

        print("Patterns:", sum([len(x) for x in listOfXPatterns]) + sum([len(x) for x in listOfYPatterns]))
        print("solved", round(solutionCount / 2), "of", GRID_SIZE * GRID_SIZE, "cells")

    paint_canvas(win, grid, canvas, repaint=True)
    print("time:", time.time() - startTime)
    win.promptClose(win.getWidth() / 2, 2)

if __name__ == '__main__':
    main()
